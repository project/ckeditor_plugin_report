<?php

namespace Drupal\Tests\ckeditor_plugin_report\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional tests for the CKEditor Plugins Report.
 *
 * @group ckeditor_plugin_report
 */
class CKEditorPluginReportTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['ckeditor_plugin_report'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests permissions.
   */
  public function testPermissions() {
    $account = $this->drupalCreateUser(['view ckeditor plugin report']);
    $this->drupalLogin($account);
    $this->drupalGet('admin/reports/ckeditor-plugins');
    $this->assertSession()->statusCodeEquals(200);

    $account = $this->drupalCreateUser([]);
    $this->drupalLogin($account);
    $this->drupalGet('admin/reports/ckeditor-plugins');
    $this->assertSession()->statusCodeEquals(403);
  }

}
