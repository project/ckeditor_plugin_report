<?php

namespace Drupal\Tests\ckeditor_plugin_report\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * FunctionalJavascript tests for the CKEditor Plugins Report.
 *
 * @group ckeditor_plugin_report
 */
class CKEditorPluginReportTest extends WebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['ckeditor_plugin_report'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $account = $this->drupalCreateUser(['view ckeditor plugin report']);
    $this->drupalLogin($account);
  }

  /**
   * Tests report for CKEditorPlugin plugins.
   */
  public function testCkEditor4Plugins() {
    $web_assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('admin/reports/ckeditor-plugins');

    $element = $page->find('xpath', '//details[contains(@class, "ckeditor4-plugins")]');
    $this->assertNull($element);

    \Drupal::service('module_installer')->install(['ckeditor']);

    $this->drupalGet('admin/reports/ckeditor-plugins');

    $page->find('xpath', '//details[contains(@class, "ckeditor4-plugins")]')->click();

    $xpath_base = '//details[contains(@class, "ckeditor4-plugins")]//tbody//td[contains(text(), "Drupal\ckeditor\Plugin\CKEditorPlugin\Internal")]/..';
    $web_assert->elementTextEquals('xpath', "$xpath_base/td", 'internal');
    $web_assert->elementTextEquals('xpath', "$xpath_base/td[2]", 'ckeditor');
    $web_assert->elementTextEquals('xpath', "$xpath_base/td[3]", 'Drupal\ckeditor\Plugin\CKEditorPlugin\Internal');
  }

  /**
   * Tests report for CKEditor5Plugin plugins.
   */
  public function testCkEditor5Plugins() {
    $web_assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('admin/reports/ckeditor-plugins');

    $element = $page->find('xpath', '//details[contains(@class, "ckeditor5-plugins")]');
    $this->assertNull($element);

    \Drupal::service('module_installer')->install(['ckeditor5']);

    $this->drupalGet('admin/reports/ckeditor-plugins');

    $page->find('xpath', '//details[contains(@class, "ckeditor5-plugins")]')->click();

    $xpath_base = '//details[contains(@class, "ckeditor5-plugins")]//tbody//td[contains(text(), "Drupal\ckeditor5\Plugin\CKEditor5Plugin\SourceEditing")]/..';
    $web_assert->elementTextEquals('xpath', "$xpath_base/td", 'ckeditor5_sourceEditing');
    $web_assert->elementTextEquals('xpath', "$xpath_base/td[2]", 'ckeditor5');
    $web_assert->elementTextEquals('xpath', "$xpath_base/td[3]", 'Drupal\ckeditor5\Plugin\CKEditor5Plugin\SourceEditing');
  }

  /**
   * Tests report for CKEditor4To5Upgrade plugins.
   */
  public function testCkEditor4to5UpgradePlugins() {
    $web_assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $this->drupalGet('admin/reports/ckeditor-plugins');

    $element = $page->find('xpath', '//details[contains(@class, "ckeditor4to5upgrade-plugins")]');
    $this->assertNull($element);

    \Drupal::service('module_installer')->install(['ckeditor5']);

    $this->drupalGet('admin/reports/ckeditor-plugins');

    $page->find('xpath', '//details[contains(@class, "ckeditor4to5upgrade-plugins")]')->click();

    $xpath_base = '//details[contains(@class, "ckeditor4to5upgrade-plugins")]//tbody//td[contains(text(), "Drupal\ckeditor5\Plugin\CKEditor4To5Upgrade\Contrib")]/..';
    $web_assert->elementTextEquals('xpath', "$xpath_base/td", 'contrib');
    $web_assert->elementTextEquals('xpath', "$xpath_base/td[2]", 'ckeditor5');
    $web_assert->elementTextEquals('xpath', "$xpath_base/td[3]", 'Drupal\ckeditor5\Plugin\CKEditor4To5Upgrade\Contrib');
  }

}
